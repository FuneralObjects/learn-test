package com.funeral.api.aliyun.po;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AliyunConfigure {

    public static String ACCESSKEY_ID;
    public static String ACCESSKEY_SECRET;

    public static String SMS_PRODUCT;
    public static String SMS_DOMAIN;
//    public static String SMS_SINGNAME;
//    public static String SMS_TEMPLATECODE;


    @Value("#{aliyunProperties['accessKeyId']}")
    public void setAccesskeyId(String accesskeyId) {
        ACCESSKEY_ID = accesskeyId;
    }

    @Value("#{aliyunProperties['accessKeySecret']}")
    public void setAccesskeySecret(String accesskeySecret) {
        ACCESSKEY_SECRET = accesskeySecret;
    }

    @Value("#{aliyunProperties['sms.product']}")
    public void setSmsProduct(String smsProduct) {
        SMS_PRODUCT = smsProduct;
    }

    @Value("#{aliyunProperties['sms.domain']}")
    public void setSmsDomain(String smsDomain) {
        SMS_DOMAIN = smsDomain;
    }

//    @Value("#{aliyunProperties['sms.signName']}")
//    public void setSmsSingname(String smsSingname) {
//        SMS_SINGNAME = smsSingname;
//    }
//
//    @Value("#{aliyunProperties['sms.templateCode']}")
//    public void setSmsTemplatecode(String smsTemplatecode) {
//        SMS_TEMPLATECODE = smsTemplatecode;
//    }
}
