package com.funeral.api.baidu.map.dto;

import java.util.List;

public class CoordConvertReturn {
    private Integer status;
    private String message;
    private List<Coord> result;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Coord> getResult() {
        return result;
    }

    public void setResult(List<Coord> result) {
        this.result = result;
    }
}
