package com.funeral.api.baidu.map.dto;

import java.util.List;

public class CoordConvertParam {
    /**
     * 需转换的源坐标，多组坐标以“；”分隔（经度，纬度）
     */
    private List<Coord> coords;
    /**
     * 开发者密钥,用户申请注册的key
     */
    private String ak;
    /**
     * 	源坐标类型：
     * 	1：GPS设备获取的角度坐标，wgs84坐标;
     * 	2：GPS获取的米制坐标、sogou地图所用坐标;
     * 	3：google地图、soso地图、aliyun地图、mapabc地图和amap地图所用坐标，国测局（gcj02）坐标;
     * 	4：3中列表地图坐标对应的米制坐标;
     * 	5：百度地图采用的经纬度坐标;
     * 	6：百度地图采用的米制坐标;
     * 	7：mapbar地图坐标;
     * 	8：51地图坐标
     */
    private Integer from;
    /**
     * 目标坐标类型：
     * 5：bd09ll(百度经纬度坐标),
     * 6：bd09mc(百度米制经纬度坐标);
     */
    private Integer to;
    /**
     * 若用户所用ak的校验方式为sn校验时该参数必须
     */
    private String sn;
    /**
     * 返回结果格式 -- 默认为json
     */
    private String output = "json";

    public List<Coord> getCoords() {
        return coords;
    }

    public void setCoords(List<Coord> coords) {
        this.coords = coords;
    }

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

}
