package com.funeral.api.baidu.map.po;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BaiduMapConfigure {

    public static String CoordConvert_URL;
    public static String AK;

    @Value("#{baiduMapProperties['coordConvert.url']}")
    public void setURL(String URL) {
        BaiduMapConfigure.CoordConvert_URL = URL;
    }


    @Value("#{baiduMapProperties['ak']}")
    public void setAK(String AK) {
        BaiduMapConfigure.AK = AK;
    }
}
