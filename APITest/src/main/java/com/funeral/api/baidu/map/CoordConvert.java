package com.funeral.api.baidu.map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.funeral.api.baidu.map.dto.CoordConvertParam;
import com.funeral.api.baidu.map.dto.CoordConvertReturn;
import com.funeral.api.baidu.map.po.BaiduMapConfigure;
import com.funeral.api.baidu.map.util.CoordConvertUtils;
import com.funeral.exception.CallException;
import com.funeral.util.HttpClientUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.springframework.util.StringUtils;

import java.io.IOException;


public class CoordConvert {

//    private static final CoordConvertConfigure CONFIGURE = null;

    /**
     * 使用CoordCovertParam类来访问百度地图坐标转换功能
     * 默认使用baiduMapProperties中的ak来访问
     * @param param
     * @return
     * @throws CallException
     */
    public static CoordConvertReturn execute(CoordConvertParam param) throws CallException{
        if(param == null){throw new CallException("param is Blank!");}
        if(StringUtils.isEmpty(param.getAk())){
            param.setAk(BaiduMapConfigure.AK);
        }
        HttpGet httpGet = CoordConvertUtils.getHttpGet(BaiduMapConfigure.CoordConvert_URL,param);
        HttpResponse response = HttpClientUtils.execute(httpGet);
        try {
            return new ObjectMapper().readValue(HttpClientUtils.simpleResult(response),CoordConvertReturn.class);
        } catch (IOException e) {
            throw new CallException(e);
        }
    }

}
