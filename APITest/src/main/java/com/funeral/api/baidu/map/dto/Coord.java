package com.funeral.api.baidu.map.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 坐标类
 */
public class Coord {

    /**
     * 纬度
     */
    @JsonProperty("y")
    private Double latitude;
    /**
     * 经度
     */
    @JsonProperty("x")
    private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
