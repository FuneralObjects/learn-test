package com.funeral.exception;

/**
 * 主要用于调用外部接口访问异常
 */
public class CallException extends Exception{
    public CallException(Exception e){
        super(e);
    }
    public CallException(String message){
        super(message);
    }
}
