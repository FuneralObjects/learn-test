package com.funeral.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext; // Spring应用上下文环境
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextUtils.applicationContext = applicationContext;
    }

    public static<T> T getBean(String name){
        return (T)SpringContextUtils.applicationContext.getBean(name);
    }
    public static<T> T getBean(Class<T> clzz){
        return SpringContextUtils.applicationContext.getBean(clzz);
    }
}
