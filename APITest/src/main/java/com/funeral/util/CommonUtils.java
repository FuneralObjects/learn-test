package com.funeral.util;

import java.util.Collection;

public class CommonUtils {
    public static boolean isBlank(Collection collection){
        return collection == null?true
                : collection.isEmpty()?true
                : !collection.contains(null)?false
                : collection.size() == 1?true:false;
    }

    public static boolean isNotBlank(Collection collection){
        return !isBlank(collection);
    }
}
