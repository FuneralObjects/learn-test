<%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/6/29
  Time: 下午10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <title>组件的简单使用</title>
</head>
<body>
    <!-- glyphicon图标的使用 -->
    <span class="glyphicon glyphicon-plus"></span>
    <span class="glyphicon glyphicon-euro"></span>
    <hr/>
    <!-- 按钮的样式 -->
    <button type="button" class="btn btn-success">成功</button>
    <button type="button" class="btn btn-info">信息</button>
    <button type="button" class="btn btn-link">链接</button>
    <button type="button" class="btn btn-block">块级按钮</button>
    <hr/>
    <!-- 按钮其他附加样式 -->
    <button type="button" class="btn btn-default btn-lg">大按钮</button>
    <button type="button" class="btn btn-default">中等按钮</button>
    <button type="button" class="btn btn-default btn-sm">小按钮</button>
    <button type="button" class="btn btn-default btn-xs">超小按钮</button>
    <hr/>
    <!-- 跨标签渲染样式 -->
    <!-- #对于只强调按钮的功能的标签，建议只用<button> -->
    <a class="btn btn-info" href="#">a标签渲染</a> <!--如果a标签并非作为超链接使用，请务必加上该属性：role="button" -->
    <input class="btn btn-info" type="button" value="input标签渲染"/>
    <input class="btn btn-danger" type="submit" value="submit"/>
    <hr/>
    <!-- 按钮图标的简单组合 -->
    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok" style="margin-right:5px;"></span>确认</button>
    <hr/>

    <div class="input-group">
        <input class="form-control" placeholder="xxxx" type="text" value="" >
    </div>
</body>
</html>
