<%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/6/30
  Time: 下午7:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <title>组件的的进阶使用</title>
</head>
<body>
    <hr/>
    <!-- 按钮式下拉菜单
        下拉菜单需要容器，即div或者其他,并且固定class为dropdown
    -->
    <div class = "dropdown">
        <!-- 下拉按钮 -->
        <button id="dropDownMenu1" <%-- 赋予其id属性，用于下方<ul>菜单列进行关联 --%>
                class ="btn btn-default dropdown-toggle" <%-- class 为 btn(按钮)  btn-default(默认类型) dropdown-toggle(下拉触发控件样式) --%>
                data-toggle="dropdown" <%-- data-toggle为切换属性，bootstrap 将该标签渲染成下拉空间 --%>
        >点击弹出下拉
            <span class="caret"></span><!-- 下三角使用 -->
        </button>
        <!-- 下拉列 -->
        <ul class="dropdown-menu" <%-- 样式为下拉菜单样式 --%>
            role="menu" <%-- 赋予菜单角色 --%>
            aria-labelledby="dropDownMenu1"> <!-- 关联下拉触犯控件标签其id值 -->
            <li class="dropdown-header">菜单标题</li>
            <li > <!-- role属性固定为presentation（介绍） -->
                <a href="#">菜单1</a>
            </li>
            <li ><a href="#">菜单2</a></li>
            <li class="divider"></li> <!-- 分割线 -->
            <li><a href="#">菜单3--------加长内容-------</a></li>
            <li class="disabled"><a href ="#">禁止菜单</a></li>
        </ul>
    </div>
    <hr/>
    <!-- 按钮组 -->
    <div class="btn-group btn-group-lg" role="group">
        <button type="button" class="btn btn-default">按钮1</button>
        <button type="button" class="btn btn-success">按钮2</button>
        <button type="button" class="btn btn-info">按钮3</button>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default">按钮1</button>
        <button type="button" class="btn btn-success">按钮2</button>
        <button type="button" class="btn btn-info">按钮3</button>
    </div>
    <div class="btn-group btn-group-sm">
        <button type="button" class="btn btn-default">按钮1</button>
        <button type="button" class="btn btn-success">按钮2</button>
        <button type="button" class="btn btn-info">按钮3</button>
    </div>

    <div class="btn-group btn-group-xs">
        <button type="button" class="btn btn-default">按钮1</button>
        <button type="button" class="btn btn-success">按钮2</button>
        <button type="button" class="btn btn-info">按钮3</button>
    </div>
    <hr/>
    <!-- 按钮工具栏 -->
    <div class="btn-toolbar">
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-info">按钮1</button>
            <button type="button" class="btn btn-info">按钮2</button>
            <button type="button" class="btn btn-info">按钮3</button>
        </div>
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-info">按钮1</button>
            <button type="button" class="btn btn-info">按钮2</button>
            <button type="button" class="btn btn-info">按钮3</button>
        </div>
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-info">按钮1</button>
            <button type="button" class="btn btn-info">按钮2</button>
            <button type="button" class="btn btn-info">按钮3</button>
        </div>
    </div>
    <hr/>
    <!-- 简单按钮组合（嵌套） -->
    <div class="btn-toolbar">
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-info">按钮1</button>
            <div class="btn-group"><!-- 不使用dropdown 转而继续使用btn-group来实现嵌套组合 -->
                <button type="button" class="btn btn-default dropdown-toggle" id="groupBtnMenu1" data-toggle="dropdown">下拉按钮<span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="groupBtnMenu1">
                    <li><a href="#">菜单1</a></li>
                    <li><a href="#">菜单2</a></li>
                </ul>
            </div>
            <button type="button" class="btn btn-primary">按钮2</button>
        </div>
    </div>
    <hr/>
    <!-- 两端对齐 -->
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <button type="button" class="btn btn-default">等宽1</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-default">等宽2</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-default">等宽3</button>
        </div>
    </div>
    <hr/>
    <!-- 分裂式下拉按钮 -->
    <div class="btn-group">
        <button type="button" class="btn btn-lg btn-default" >下拉分裂式按钮</button>
        <button type="button" class="btn btn-lg btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span>&nbsp;</button>
        <ul class="dropdown-menu"  role="menu">
            <li><a href="#">test1</a></li>
            <li><a href="#">test2</a></li>
            <li><a href="#">test3</a></li>
            <li><a href="#">test4</a></li>
        </ul>
    </div>
    <hr/>
    <!-- 分列式上拉按钮 -->
    <div class="btn-group dropup">
        <button type="button" class="btn btn-sm btn-default" >上拉分裂式按钮</button>
        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span>&nbsp;</button>
        <ul class="dropdown-menu"  role="menu">
            <li><a href="#">test1</a></li>
            <li><a href="#">test2</a></li>
            <li><a href="#">test3</a></li>
            <li><a href="#">test4</a></li>
        </ul>
    </div>
</body>
</html>
