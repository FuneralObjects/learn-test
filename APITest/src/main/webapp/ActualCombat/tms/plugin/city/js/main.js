/**
 * Created by sicilin on 2017/7/1.
 */
$(function () {
    'use strict';

    var $citypicker3 = $('#city-picker3');

    $('#reset').click(function () {
        $citypicker3.citypicker('reset');
    });

    $('#destroy').click(function () {
        $citypicker3.citypicker('destroy');
    });

});