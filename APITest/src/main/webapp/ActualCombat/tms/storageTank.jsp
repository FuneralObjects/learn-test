<%--
  Created by IntelliJ IDEA.
  User: sicilin
  Date: 2017/7/1
  Time: 10:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <title>储罐查询页面</title>

    <!-- bootstrap 城市选择插件引入 -->
    <link rel="stylesheet" href="plugin/city/css/city-picker.css"/>
    <script type="text/javascript" src="plugin/city/js/city-picker.data.js"></script>
    <script type="text/javascript" src="plugin/city/js/city-picker.js"></script>
    <script type="text/javascript" src="plugin/city/js/main.js"></script>

    <!-- bootstrap table引入 -->
    <link rel="stylesheet" href="plugin/table/bootstrap-table.css"/>
    <script type="text/javascript" src="plugin/table/bootstrap-table.js"></script>
    <!-- bootstrap table Local本地化文件 -->
    <script type="text/javascript" src="plugin/table/locale/bootstrap-table-zh-CN.js"></script>

</head>
<body>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">搜索</div>
            <div class="panel-body">
                <div class="input-group" style="position: relative;float: left;margin-right:10px;">
                    <input type="text" style="width:200px;" placeholder="名称" class="form-control">
                </div>
                <div style="float:left;">
                    <div style="position: relative;float: left;">
                        <input style="width:360px;" id="city-picker3" class="form-control" readonly type="text" data-toggle="city-picker">
                    </div>
                    <button class="btn btn-warning btn-sm" id="reset" type="button" style="float: left;">重置选择</button>
                </div>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">储罐信息</div>
            <table id = "storageTankTable"></table>
        </div>
    </div>
</body>
<script type="text/javascript">
    function init(){
        initTable();
    }

    function initTable(){
        $("#storageTankTable").bootstrapTable({
            url:"json/storageTank.json"
            ,columns:[{field:'id',title:'Item ID'}
                    ,{field:'name',title:'名称'}
                    ,{field:'over',title:'剩余量',formatter:progressFormatter}
                    ,{field:'state',title:'状态'}]
            ,pagination:true
            ,pageSize:2
            ,pageNumber:1
            ,pageList:[2,3,4]
            ,search:true
            ,searchOnEnterKey:true
    //        ,minimumCountColumns:3
    //        ,showFooter:true
    //        ,showToggle:true
    //        ,showPaginationSwitch:true
            ,idField:"id"
            ,showRefresh:true
        });
    }

    function progressFormatter(value,row,index){
        return makeProgress(50);
    }

    function makeProgress(value){
        var progress = $('<div></div>');
        progress.addClass('progress');
        progress.css({'margin-bottom':'0px'});
        var progress_bar = $('<div></div>');
        progress_bar.addClass('progress-bar');
        progress_bar.addClass('progress-bar-success');
        progress_bar.attr({"role":"progressbar","aria-valuenow":value,"aria-valuemin":"0","aria-valuemax":"100"});
        progress_bar.css({"width":value+'%'});
        progress_bar.append('<span>'+value+'%</span>');
        progress.append(progress_bar);
        return progress.prop("outerHTML");
    }

</script>
<script type="text/javascript">
    init();

</script>

</html>
