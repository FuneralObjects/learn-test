<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.funeral.diyaccount.common.properties.Version" %><%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/2/9
  Time: 上午9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
    Version v = context.getBean("version",Version.class);
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String version = v.getVersion();
%>
<link rel="stylesheet" type="text/css" href = "<%=basePath%>script/easyUI/jquery-easyui-1.5.1/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>script/easyUI/jquery-easyui-1.5.1/themes/icon.css">
<script type = "text/javascript" src = "<%=basePath%>script/easyUI/jquery-easyui-1.5.1/jquery.min.js"></script>
<script type = "text/javascript" src = "<%=basePath%>script/easyUI/jquery-easyui-1.5.1/jquery.easyui.min.js"></script>
<script type = "text/javascript" src = "<%=basePath%>script/easyUI/jquery-easyui-1.5.1/jquery.easyui.mobile.js"></script>
<script type = "text/javascript" >
    var basePath = '<%=basePath%>';
    var path = '<%=path%>';
    var version = '<%=version%>';
</script>