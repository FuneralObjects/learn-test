<%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/5/19
  Time: 上午11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/common.jsp"%>
<%
    String random = String.valueOf(Math.random());
%>
<html>
<head>
    <title>菜单管理</title>
</head>
    <script type="text/javascript" src="<%=basePath%>script/easyUI/jquery-easyui-1.5.1/extension/treegrid-dnd.js" ></script>
    <script type="text/javascript" src="<%=basePath%>common/easyui-common.js"></script>
    <script type="text/javascript" src = "<%=basePath%>pages/menu/menu-index.js?<%=random%>" ></script>
    <script type="text/javascript">
        $(document).ready(function(){
            initTreegrid();
            initButton();
        });





    </script>
<body>
    <div id = "menu-toolbar">
        <a id = "add-menu" >添加菜单</a>
        <a id = "add-menuFolder">添加菜单夹</a>
        <a id = "save-menu">保存更改</a>
        <a id = "delete-menu">删除菜单(夹)</a>
    </div>
    <table id = "menu-treegrid" ></table>
</body>
</html>
