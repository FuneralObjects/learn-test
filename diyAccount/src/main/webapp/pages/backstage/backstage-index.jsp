<%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/5/11
  Time: 下午2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/common.jsp"%>
<html>
<head>
    <title>后台管理页面</title>
    <script type = "text/javascript">
        $(document).ready(function(){
            initTabs();
            initMenu();
        });

        function initTabs(){
            $('#centerTabs').tabs({
                fit:true
                ,border:true
            });
        }

        function initMenu(){
            $('#westMenu').menu({
                inline:true
                ,fit:true
            });

            //TODO:测试用
            $('#westMenu').menu('appendItem',{
                text:'URL管理'
                ,onclick:function(){
                    addTab('urlTab','URL管理',basePath + 'pages/url/url-index.jsp');
                }
            });
            $('#westMenu').menu('appendItem',{
                text:'URL管理1'
                ,onclick:function(){
                    addTab('urlTab1','URL管理1',basePath + 'pages/url/url-index.jsp');
                }
            });
        }
        function addTab(id,title,url){
            if($('#centerTabs').tabs('exists',title)){
                $('#centerTabs').tabs('select',title);
                refreshTab(id,title);
                return;
            }

            var iframe = $('<iframe id ="'+id+'_iframe" src = "'+url+'" style = "border:none;width:100%;height:100%;"></iframe>');

            $('#centerTabs').tabs('add',{
                id:id
                ,title:title
                ,content :iframe
                ,closable:true
                ,fit:true
            });

//            var tab = $('#'+id+'_iframe').parent();
//            var theight = tab.css('height');
//            var twidth = tab.css('width');
//            iframe.css({'width':twidth,'height':theight});
        }

        function refreshTab(id,tabTitle){
            if(!$('#centerTabs').tabs('exists',tabTitle)){
                return;
            }
            var currentTab = $('#centerTabs').tabs('getTab',tabTitle);
            var iframeJq = currentTab.find('#'+id+'_iframe');
            iframeJq.attr('src',iframeJq.attr('src'));

        }

    </script>
</head>
<body>
    <div id = "layoutDiv" class="easyui-layout" data-options="fit:true" >
        <div id = "northBar" style ="height: 100px;"
            data-options = "title:'后台管理',region:'north'">
        </div>
        <div id = "westBar" style = "width: 200px;"
             data-options="title:'菜单',region:'west'" >
            <div id="westMenu" style="width:200px;">
            </div>
        </div>
        <div id = "centerBar" style = "border:none;"
            data-options = "region:'center',fit:false">
            <div id = "centerTabs">
            </div>
        </div>
    </div>
</body>
</html>
