<%--
  Created by IntelliJ IDEA.
  User: peizangpin
  Date: 2017/5/5
  Time: 上午9:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/common.jsp"%>
<%
    Double random = Math.random();
%>
<!DOCTYPE html>
<html>
<head>
    <title>地址管理</title>

    <style type = "text/css">
        .urlBtn{
            margin:4px 0px;
        }
        .w100{
            /*display:block;*/
            width:100px;
        }
    </style>
    <!-- TODO:作为random加载方便测试去缓存 -->
    <script type="text/javascript" src = "<%=basePath%>pages/url/url-index.js?version=<%=random%>" ></script>
    <script type="text/javascript" >
        $(document).ready(function(){
            initDatagrid();
            initTooBar();
            initSearchBox();

        });
    </script>
</head>
<body>

    <div id = "url-toolBar">
        <a class = "urlBtn" id = "addBtn" >添加</a>
        <a class = "urlBtn" id = "delBtn">删除</a>
        <a class = "urlBtn" id = "editBtn">编辑</a>
        <br>
        <input id = "url-searchBox" type = "text" style="margin:4px 0px;">
        <%--<a id = "searchBtn">搜索</a>--%>
    </div>

    <!-- URL数据表格 -->
    <div id = "url-datagrid"></div>

    <!-- 添加URL弹窗 -->
    <div id = "url-add-dialog" style = "display: none;" data-options="title:'添加URL'">
        <table>
            <thead></thead>
            <tbody>
                <tr>
                    <td><span class='w100'>类型：</span></td>
                    <td>
                        <select id = 'url-add-type'>
                            <option value = "1">1</option>
                            <option value = "2">2</option>
                            <option value = "3">3</option>
                            <option value = "4">4</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class='w100'>Action名称：</span></td>
                    <td><input id = "url-add-action" type = "text" /></td>
                </tr>
                <tr>
                    <td><span class='w100'>Do方法</span></td>
                    <td><input id = "url-add-do" type="text" /></td>
                </tr>
                <%--<tr>--%>
                    <%--<td><span class='w100'>完整路径</span></td>--%>
                    <%--<td><input id ="url-add-completeUrl" type = "text"/></td>--%>
                <%--</tr>--%>
            </tbody>
        </table>
    </div>

    <!-- 编辑URL -->
    <div id = "url-edit-dialog" style = "display: none;" data-options="title:'添加URL'">
        <table>
            <thead></thead>
            <tbody>
            <tr>
                <td><span class='w100'>类型：</span></td>
                <td>
                    <select id = 'url-edit-type'>
                        <option value = "1">1</option>
                        <option value = "2">2</option>
                        <option value = "3">3</option>
                        <option value = "4">4</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><span class='w100'>Action名称：</span></td>
                <td><input id = "url-edit-action" type = "text" /></td>
            </tr>
            <tr>
                <td><span class='w100'>Do方法</span></td>
                <td><input id = "url-edit-do" type="text" /></td>
            </tr>
            <tr>
                <td><span class='w100'>完整路径</span></td>
                <td><input id ="url-edit-completeUrl" type = "text"/></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id = "msgDialog" style = "display: none;">

    </div>
</body>
</html>
