/**
 * Created by peizangpin on 2017/5/12.
 */
function initDatagrid(){
    $('#url-datagrid').datagrid({
        url : basePath + 'url-action/query-list.html'
        ,fit:true
        ,method:'post'
        //TODO:测试数据
        // data:[{"urlid":'11111',"urltype":"2","actionName":'testAction',"actionDo":"testDo","completeUrl":"testAction/testDo","enable":"1"}]
        ,title:'地址列表'
        ,singleSelect : false
        ,toolbar:'#url-toolBar'
        ,pagination:true
        ,rownumbers:true
        ,pageSize:20
        ,pageNumber:1
        ,pageList:[20,50,100]
        ,frozenColumns : [[
            {field : 'ck' ,title : '选择' ,width:80 , checkbox : true}
        ]]
        ,columns:[[
            {field:'urlid',title:'id',width:150}
            ,{field:'urltype',title:'类型',width:50}
            ,{field:'actionName',title:'Action',width:150}
            ,{field:'actionDo',title:'Do',width:200}
            ,{field:'completeUrl',title:'完整URL',width:300}
            ,{field:'enable',title:'是否启用',width:100}
        ]]
    });
}

function initTooBar(){
    $('#addBtn').linkbutton({
        iconCls:'icon-Add'
        ,onClick: openAddDialog
    });
    $('#delBtn').linkbutton({
        iconCls:'icon-Remove'
        ,onClick: delUrl
    });
    $('#editBtn').linkbutton({
        iconCls:'icon-Edit'
        ,onClick:openEditDialog
    });

//            $('#searchBtn').linkbutton({
//                iconCls:'icon-search'
//            });
}

function initSearchBox(){
    $('#url-searchBox').textbox({
        prompt: "关键字搜索"
        ,width: 300
        ,buttonText:'搜索'
        ,buttonIcon:'icon-search'
    });
}

var openAddDialogBtns = [
    {text:'添加',iconCls:'icon-ok',onClick:addUrl}
    ,{text:'重置',iconCls:'icon-reload'}
];
var openEditDialogBtns = [{text:'添加',iconCls:'icon-ok'}
    ,{text:'重置',iconCls:'icon-reload'}]
function openAddDialog(){

    $('#url-add-dialog').dialog({
        title:'添加URL'
        ,modal:true
        ,cache:false
        ,buttons:openAddDialogBtns
        ,resizable:true
    });
}

function openEditDialog(){
    $('#url-edit-dialog').dialog({
        title:'添加URL'
        ,modal:true
        ,cache:false
        ,buttons:openEditDialogBtns
        ,resizable:true
    });
}
function addUrl(){
    var urlType = $('#url-add-type').val();
    var actionName = $('#url-add-action').val();
    var actionDo = $('#url-add-do').val();
    // var completeUrl = $('#url-add-completeUrl').val();
    var completeUrl = actionName + '/' + actionDo;

    $.ajax({
        url:basePath + 'url-action/add-url.html'
        ,type:'post'
        ,dataType:'json'
        ,data:{urlType:urlType,actionName:actionName,actionDo:actionDo,completeUrl:completeUrl}
        ,success:function(data){
            console.info(data);
            if(data['error'] == '1'){
                alert(data['errorMsg']);
            }else {
                alert('success');
            }
        }
        ,error:function(){
            alert('error');
        }
    });
}

function delUrl(){
    var checkedrow = $('#url-datagrid').datagrid('getChecked');
    if(checkedrow.length == 0){
        return;
    }
    var idArr = new Array();
    for(var key = 0; key < checkedrow.length; key++){
        idArr.push(checkedrow[key]['urlid']);
    }

    var buttons = [{
        text:'确认'
        ,iconCls:'icon-ok'
        ,onClick:function(){
            $.ajax({
                url:basePath+'url-action/del-url.html'
                ,type:'post'
                ,dataType:'json'
                ,data:{idArr:idArr.join(",")}
                ,success:function (data) {
                    if(data['error'] == '1'){
                        alert(data['errorMsg']);
                    }else{
                        alert('success');
                    }
                }
                ,error:function(){
                    alert('error');
                }
            });
        }
    }];
    var msg = '将要删除'+ idArr.length +'条数据!';
    openMsgDialg('删除url',msg,buttons);

}

function openMsgDialg(title,message,buttons){
    $('#msgDialog').dialog({
        title:title
        ,modal:true
        ,resizable:true
        ,cache:false
        ,buttons:buttons
        ,content:message

    });
}

