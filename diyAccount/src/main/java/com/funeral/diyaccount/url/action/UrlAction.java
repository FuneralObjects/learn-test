package com.funeral.diyaccount.url.action;

import com.funeral.diyaccount.base.exception.AjaxMessageException;
import com.funeral.diyaccount.common.ajax.pojo.AjaxInfo;
import com.funeral.diyaccount.common.ajax.util.AjaxInfoUtils;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import com.funeral.diyaccount.common.page.util.PageUtils;
import com.funeral.diyaccount.common.properties.Version;
import com.funeral.diyaccount.url.entity.TDAUrl;
import com.funeral.diyaccount.url.service.UrlService;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;

/**
 * Created by peizangpin on 2017/5/5.
 */
@Controller
@RequestMapping("/url-action")
public class UrlAction {

    @Resource
    private UrlService urlService;

    @RequestMapping(value = "/query-list.html",method = RequestMethod.POST)
    public void queryUrlList(HttpServletRequest request, HttpServletResponse response)throws Exception{
        String jsonFilterStr = request.getParameter("filter");
        JSONObject jsonFilter = null;
        if(StringUtils.isBlank(jsonFilterStr)){
            jsonFilter = new JSONObject();
        }else{
            jsonFilter = JSONObject.fromObject(jsonFilterStr);
        }
        PageObject<TDAUrl> pageObject = new PageObject<TDAUrl>();
        pageObject = PageUtils.initPageObject(request);

        pageObject = urlService.queryUrlListByPage(pageObject, jsonFilter);

        response.getWriter().printf(PageUtils.ToEasyUIDatagridJSON(pageObject));
    }


    @RequestMapping(value = "/test.html",method = RequestMethod.GET)
    public void test(HttpServletRequest request, HttpServletResponse response)throws Exception{
        TDAUrl url = new TDAUrl();
        Class clzz = url.getClass();
        Field[] fs = clzz.getDeclaredFields();
        for(Field f:fs){
            System.out.println(f.getName());
        }

    }

    @RequestMapping(value ="/add-url.html",method = RequestMethod.POST)
    public void addUrl(HttpServletRequest request,HttpServletResponse response)throws Exception{
        TDAUrl url = new TDAUrl();
        String urlTypeStr = request.getParameter("urlType");
        String actionNameStr = request.getParameter("actionName");
        String actionDoStr = request.getParameter("actionDo");
        String completeUrlStr =request.getParameter("completeUrl");

        if(StringUtils.isNotBlank(urlTypeStr) && StringUtils.isNumeric(urlTypeStr)){
            url.setUrltype(Integer.valueOf(urlTypeStr));
        }
        if(StringUtils.isNotBlank(actionNameStr)){
            url.setActionName(actionNameStr);
        }
        if(StringUtils.isNotBlank(actionDoStr)) {
            url.setActionDo(actionDoStr);
        }
        if(StringUtils.isNotBlank(completeUrlStr)){
            url.setCompleteUrl(completeUrlStr);
        }

        AjaxInfo ajaxInfo = new AjaxInfo();
        ajaxInfo = AjaxInfoUtils.initAjaxInfo(ajaxInfo);
        try{
            Long urlid = urlService.saveUrl(url);
            ajaxInfo.setSuccess(urlid.toString());
        }catch (AjaxMessageException ame) {
            ajaxInfo.setError(1);
            ajaxInfo.setErrorMsg(ame.getMessage());
        }

        response.getWriter().printf(AjaxInfoUtils.toJSONString(ajaxInfo));


    }

}
