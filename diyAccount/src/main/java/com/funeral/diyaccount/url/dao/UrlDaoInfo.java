package com.funeral.diyaccount.url.dao;

import com.funeral.diyaccount.url.entity.TDAUrl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by peizangpin on 2017/5/4.
 */
@Component
public class UrlDaoInfo {
    @Value("${urlDao.tableName}")
    private String tableName;

    @Value("${urlDao.idName}")
    private String idName;

    @Value("${urlDao.sequenceName}")
    private String sequenceName;

    private final RowMapper<TDAUrl> rowMapper = new RowMapper<TDAUrl>() {
        public TDAUrl mapRow(ResultSet resultSet, int i) throws SQLException {
            TDAUrl url = new TDAUrl();
            url.setActionDo(resultSet.getString("actiondo"));
            url.setActionName(resultSet.getString("actionname"));
            url.setCompleteUrl(resultSet.getString("completeurl"));
            url.setEnable(resultSet.getInt("enable"));
            url.setUrltype(resultSet.getInt("urltype"));
            url.setUrlid(resultSet.getLong("urlid"));
            return url;
        }
    };


    public String getTableName() {
        return tableName;
    }

    public String getIdName() {
        return idName;
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public RowMapper<TDAUrl> getRowMapper() {
        return rowMapper;
    }
}
