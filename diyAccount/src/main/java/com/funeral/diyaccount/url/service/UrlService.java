package com.funeral.diyaccount.url.service;

import com.funeral.diyaccount.base.exception.AjaxMessageException;
import com.funeral.diyaccount.base.exception.DaoException;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import com.funeral.diyaccount.url.dao.UrlDao;
import com.funeral.diyaccount.url.entity.TDAUrl;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * Created by peizangpin on 2017/5/8.
 */
@Service
public class UrlService {

    @Resource
    private UrlDao urlDao;

    public PageObject<TDAUrl> queryUrlListByPage(PageObject<TDAUrl> pageObject,JSONObject jsonFilter) throws AjaxMessageException{
        try {
            pageObject = urlDao.queryUrlListByPage(pageObject, jsonFilter);
        }catch (DaoException de){
            de.printStackTrace();
            throw new AjaxMessageException(de.getMessage());
        }
        return pageObject;
    }

    public Long saveUrl(TDAUrl url) throws AjaxMessageException{
        Long urlid = null;
        try {
            urlid = urlDao.saveUrl(url);
        }catch (DaoException de){
            de.printStackTrace();
            throw new AjaxMessageException(de.getMessage());
        }
        return urlid;
    }

    public void deleteUrl(Collection<Long> ids) throws AjaxMessageException{
        try {
            for (Long id : ids) {
                urlDao.delUrl(id);
            }
        }catch (DaoException de){
            de.printStackTrace();
            throw new AjaxMessageException(de.getMessage());
        }

    }
}
