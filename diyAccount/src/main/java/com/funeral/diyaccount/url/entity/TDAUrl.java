package com.funeral.diyaccount.url.entity;

/**
 * Created by peizangpin on 2017/5/4.
 */
public class TDAUrl {
    private Long urlid;
    private Integer urltype;
    private String actionName;
    private String actionDo;
    private String completeUrl;
    private Integer enable;

    public TDAUrl(){
        this.enable = 1;
        this.urltype = 1;
    }

    public Long getUrlid() {
        return urlid;
    }

    public void setUrlid(Long urlid) {
        this.urlid = urlid;
    }

    public Integer getUrltype() {
        return urltype;
    }

    public void setUrltype(Integer urltype) {
        this.urltype = urltype;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionDo() {
        return actionDo;
    }

    public void setActionDo(String actionDo) {
        this.actionDo = actionDo;
    }

    public String getCompleteUrl() {
        return completeUrl;
    }

    public void setCompleteUrl(String completeUrl) {
        this.completeUrl = completeUrl;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
}
