package com.funeral.diyaccount.base.exception;

/**
 * Created by peizangpin on 2017/5/22.
 */
public class ServiceException extends Exception{
    public ServiceException(){
        super();
    }
    public ServiceException(String message){
        super(message);
    }
}
