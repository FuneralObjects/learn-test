package com.funeral.diyaccount.base.exception;

/**
 * Created by peizangpin on 2017/4/24.
 */
public class BaseDaoException extends DaoException{
    public BaseDaoException() {
        super();
    }

    public BaseDaoException(String message) {
        super(message);
    }
}
