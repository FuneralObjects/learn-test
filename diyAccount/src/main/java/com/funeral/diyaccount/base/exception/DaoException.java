package com.funeral.diyaccount.base.exception;

/**
 * Created by peizangpin on 2017/5/18.
 */
public class DaoException extends ServiceException {
    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }
}
