package com.funeral.diyaccount.base.dao;

import org.springframework.jdbc.core.RowMapper;

/**
 * Created by peizangpin on 2017/5/20.
 */
public abstract class BaseDaoInfo<T> {
    private String tableName;
    private String idName;
    private String sequenceName;

    private RowMapper<T> rowMapper;

    protected void setTableName(String tableName){
        this.tableName = tableName;
    }
    protected void setIdName(String idName){
        this.idName = idName;
    }

    protected void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    protected void setRowMapper(RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;
    }

    protected abstract void initRowMapper();

    public String getTableName() {
        return tableName;
    }

    public String getIdName() {
        return idName;
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public RowMapper<T> getRowMapper() {
        return rowMapper;
    }
}
