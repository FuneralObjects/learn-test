package com.funeral.diyaccount.base.dao;

import com.funeral.diyaccount.base.exception.DaoException;

/**
 * Created by peizangpin on 2017/5/22.
 */
public interface IDaoInfo<T> {
    void intiDaoInfo(BaseDaoInfo<T> daoInfo) throws DaoException;
}
