package com.funeral.diyaccount.base.exception;

/**
 * Created by peizangpin on 2017/5/5.<br>
 *     用于返回给前台错误信息的异常类
 */
public class AjaxMessageException extends Exception{
    public AjaxMessageException(String message){
        super(message);
    }
}
