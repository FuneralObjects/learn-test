package com.funeral.diyaccount.object.dao;

import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import com.funeral.diyaccount.object.entity.TDAObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by peizangpin on 2017/5/20.
 */
@Component
public class ObjectDaoInfo extends BaseDaoInfo<TDAObject>{

    @Value("${objectDao.tableName}")
    @Override
    protected void setTableName(String tableName) {
        super.setTableName(tableName);
    }

    @Value("${objectDao.idName}")
    @Override
    protected void setIdName(String idName) {
        super.setIdName(idName);
    }

    @Value("${objectDao.sequenceName}")
    @Override
    protected void setSequenceName(String sequenceName) {
        super.setSequenceName(sequenceName);
    }

    @PostConstruct
    @Override
    protected void initRowMapper() {
        RowMapper<TDAObject> rowMapper = new RowMapper<TDAObject>(){
            public TDAObject mapRow(ResultSet resultSet, int i) throws SQLException {
                TDAObject object = new TDAObject();
                object.setObjectid(resultSet.getLong("objectid"));
                object.setObjectCode(resultSet.getString("objectcode"));
                object.setObjectName(resultSet.getString("objectname"));
                object.setObjectType(resultSet.getInt("objecttype"));
                object.setGeneration(resultSet.getInt("generation"));
                object.setHasChildren(resultSet.getInt("hasChildren"));
                object.setParentid(resultSet.getLong("parentid"));
                object.setTotalCode(resultSet.getString("totalcode"));
                object.setEnable(resultSet.getInt("enable"));
                return object;
            }
        };
        super.setRowMapper(rowMapper);
    }
}
