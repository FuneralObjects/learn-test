package com.funeral.diyaccount.object.dao;

import com.funeral.diyaccount.base.dao.BaseDao;
import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import com.funeral.diyaccount.base.dao.IDaoInfo;
import com.funeral.diyaccount.base.exception.DaoException;
import com.funeral.diyaccount.object.entity.TDAObject;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by peizangpin on 2017/5/20.
 */
@Repository
public class ObjectDao extends BaseDao<TDAObject,Long> implements IDaoInfo<TDAObject>{

    @Resource(name = "objectDaoInfo")
    public void intiDaoInfo(BaseDaoInfo<TDAObject> daoInfo) throws DaoException {
        super.setDaoInfo(daoInfo);
    }

    public Long saveObject(TDAObject object) throws DaoException{
       return super.saveEntity(object);
    }

    public void delObject(Long objectid) throws DaoException{
        super.delEntity(objectid);
    }
}
