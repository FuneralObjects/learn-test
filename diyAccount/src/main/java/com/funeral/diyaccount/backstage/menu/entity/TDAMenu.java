package com.funeral.diyaccount.backstage.menu.entity;

import java.util.Date;

/**
 * Created by peizangpin on 2017/5/22.
 */
public class TDAMenu {
    /**
     * 菜单id
     */
    private Long menuid;
    /**
     * 菜单编号
     */
    private String menucode;
    /**
     * 菜单名称
     */
    private String menuname;
    /**
     * 是否为菜单夹
     */
    private Integer menuFolder;
    /**
     * 菜单地址
     */
    private String menuUrl;
    /**
     * 父级菜单id
     */
    private Long parentid;
    /**
     * 是否启用
     */
    private Integer enable;
    /**
     * 描述
     */
    private String description;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 显示顺序
     */
    private Integer displayorder;


    public Long getMenuid() {
        return menuid;
    }

    public void setMenuid(Long menuid) {
        this.menuid = menuid;
    }

    public String getMenucode() {
        return menucode;
    }

    public void setMenucode(String menucode) {
        this.menucode = menucode;
    }

    public Integer getMenuFolder() {
        return menuFolder;
    }

    public void setMenuFolder(Integer menuFolder) {
        this.menuFolder = menuFolder;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getDisplayorder() {
        return displayorder;
    }

    public void setDisplayorder(Integer displayorder) {
        this.displayorder = displayorder;
    }
}
