package com.funeral.diyaccount.backstage.menu.dao;

import com.funeral.diyaccount.backstage.menu.entity.TDAMenu;
import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by peizangpin on 2017/5/22.
 */
@Component
public class MenuDaoInfo extends BaseDaoInfo<TDAMenu> {

    @PostConstruct
    protected void initRowMapper() {
        RowMapper<TDAMenu> rowMapper = new RowMapper<TDAMenu>() {
            public TDAMenu mapRow(ResultSet resultSet, int i) throws SQLException {
                TDAMenu menu = new TDAMenu();
                menu.setMenuid(resultSet.getLong("menuid"));
                menu.setParentid(resultSet.getLong("parentid"));
                menu.setEnable(resultSet.getInt("enable"));
                menu.setMenucode(resultSet.getString("menucode"));
                menu.setMenuFolder(resultSet.getInt("menufolder"));
                menu.setMenuUrl(resultSet.getString("menuUrl"));
                menu.setMenuname(resultSet.getString("menuname"));
                menu.setDescription(resultSet.getString("description"));
                menu.setCreateTime(resultSet.getDate("createtime"));
                return menu;
            }
        };
        super.setRowMapper(rowMapper);
    }

    @Value(value="${menuDao.tableName}")
    @Override
    protected void setTableName(String tableName) {
        super.setTableName(tableName);
    }

    @Value(value="${menuDao.idName}")
    @Override
    protected void setIdName(String idName) {
        super.setIdName(idName);
    }

    @Value(value="${menuDao.sequenceName}")
    @Override
    protected void setSequenceName(String sequenceName) {
        super.setSequenceName(sequenceName);
    }
}
