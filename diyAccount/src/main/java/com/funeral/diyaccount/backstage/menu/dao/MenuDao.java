package com.funeral.diyaccount.backstage.menu.dao;

import com.funeral.diyaccount.backstage.menu.entity.TDAMenu;
import com.funeral.diyaccount.base.dao.BaseDao;
import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import com.funeral.diyaccount.base.dao.IDaoInfo;
import com.funeral.diyaccount.base.exception.DaoException;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peizangpin on 2017/5/22.
 */
@Repository
public class MenuDao extends BaseDao<TDAMenu,Long> implements IDaoInfo<TDAMenu> {

    @Resource(name = "menuDaoInfo")
    public void intiDaoInfo(BaseDaoInfo<TDAMenu> daoInfo) throws DaoException {
        super.setDaoInfo(daoInfo);
    }


    public Long saveMenu(TDAMenu menu) throws DaoException{
        return super.saveEntity(menu);
    }

    public void delMenu(Long menuid) throws DaoException{
        super.delEntity(menuid);
    }


    public PageObject<TDAMenu> queryMenu(PageObject<TDAMenu> pageObject, JSONObject jsonFilter) throws DaoException{
        if(pageObject == null){throw new DaoException("Invalid pageObject!");}
        List<Object> paramerters = new ArrayList<Object>();
        //TODO:添加查询条件
        StringBuffer sql = new StringBuffer();
        sql.append("select * from ");sql.append(this.getDaoInfo().getTableName());
        return super.queryListSqlWithPage(sql.toString(),pageObject,paramerters.toArray());
    }
}
