package com.funeral.diyaccount.backstage.menu.vo.easyui;

import com.funeral.diyaccount.backstage.menu.entity.TDAMenu;

/**
 * Created by peizangpin on 2017/5/22.
 */
public class MenuTreegridVO extends TDAMenu{
    private Long _parentId;
    private String state;

    public Long get_parentId() {
        return _parentId;
    }

    public void set_parentId(Long _parentId) {
        this._parentId = _parentId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
