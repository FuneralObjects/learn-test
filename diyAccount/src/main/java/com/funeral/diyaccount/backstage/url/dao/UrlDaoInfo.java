package com.funeral.diyaccount.backstage.url.dao;

import com.funeral.diyaccount.backstage.url.entity.TDAUrl;
import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by peizangpin on 2017/5/4.
 */
@Component
public class UrlDaoInfo extends BaseDaoInfo<TDAUrl>{

    @Value("${urlDao.tableName}")
    @Override
    protected void setTableName(String tableName) {
        super.setTableName(tableName);;
    }

    @Value("${urlDao.idName}")
    @Override
    protected void setIdName(String idName) {
        super.setIdName(idName);
    }

    @Value("${urlDao.sequenceName}")
    @Override
    protected void setSequenceName(String sequenceName) {
        super.setSequenceName(sequenceName);
    }

    @PostConstruct
    @Override
    protected void initRowMapper() {
        RowMapper<TDAUrl> rowMapper = new RowMapper<TDAUrl>() {
            public TDAUrl mapRow(ResultSet resultSet, int i) throws SQLException {
                TDAUrl url = new TDAUrl();
                url.setActionDo(resultSet.getString("actiondo"));
                url.setActionName(resultSet.getString("actionname"));
                url.setCompleteUrl(resultSet.getString("completeurl"));
                url.setEnable(resultSet.getInt("enable"));
                url.setUrltype(resultSet.getInt("urltype"));
                url.setUrlid(resultSet.getLong("urlid"));
                return url;
            }
        };
        super.setRowMapper(rowMapper);
    }


//    private String tableName;
//
//
//    private String idName;
//
//
//    private String sequenceName;

//    private final RowMapper<TDAUrl> rowMapper = new RowMapper<TDAUrl>() {
//        public TDAUrl mapRow(ResultSet resultSet, int i) throws SQLException {
//            TDAUrl url = new TDAUrl();
//            url.setActionDo(resultSet.getString("actiondo"));
//            url.setActionName(resultSet.getString("actionname"));
//            url.setCompleteUrl(resultSet.getString("completeurl"));
//            url.setEnable(resultSet.getInt("enable"));
//            url.setUrltype(resultSet.getInt("urltype"));
//            url.setUrlid(resultSet.getLong("urlid"));
//            return url;
//        }
//    };


}
