package com.funeral.diyaccount.backstage.menu.service;

import com.funeral.diyaccount.backstage.menu.dao.MenuDao;
import com.funeral.diyaccount.backstage.menu.entity.TDAMenu;
import com.funeral.diyaccount.base.exception.ServiceException;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by peizangpin on 2017/5/22.
 */
@Service
public class MenuService {

    @Resource
    private MenuDao menuDao;


    public Long saveMenu(TDAMenu menu) throws ServiceException{
        return menuDao.saveMenu(menu);
    }

    public PageObject<TDAMenu> queryMenu(PageObject<TDAMenu> pageObject, JSONObject jsonObject) throws ServiceException{
        return menuDao.queryMenu(pageObject,jsonObject);
    }

}
