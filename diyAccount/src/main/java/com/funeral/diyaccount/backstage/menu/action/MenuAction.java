package com.funeral.diyaccount.backstage.menu.action;

import com.funeral.diyaccount.backstage.menu.entity.TDAMenu;
import com.funeral.diyaccount.backstage.menu.service.MenuService;
import com.funeral.diyaccount.backstage.menu.util.MenuUtils;
import com.funeral.diyaccount.backstage.menu.vo.easyui.MenuTreegridVO;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import com.funeral.diyaccount.common.page.util.PageUtils;
import com.funeral.diyaccount.common.param.util.ParamUtils;
import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by peizangpin on 2017/5/22.
 */
@Controller
@RequestMapping("/menu-action")
public class MenuAction {

    @Resource
    private MenuService menuService;

    @RequestMapping(value="/save-menu.html",method = RequestMethod.POST)
    public void saveMenu(HttpServletRequest request, HttpServletResponse response) throws Exception{

    }

    @RequestMapping(value ="/query-page-menu.html",method = RequestMethod.GET)
    public void queryMenu(HttpServletRequest request,HttpServletResponse response) throws Exception{
        PageObject<TDAMenu> pageObject = PageUtils.initPageObject(request);
        JSONObject jsonFilter = ParamUtils.getJSONFilter(request);
        pageObject = menuService.queryMenu(pageObject,jsonFilter);
        PageObject<MenuTreegridVO> pageObjectVO = new PageObject<MenuTreegridVO>();
        BeanUtils.copyProperties(pageObject,pageObjectVO);
        pageObjectVO.setRowsList(MenuUtils.changeToMenuTreegridVOList(pageObject.getRowsList()));


        JSONObject jsonResult = PageUtils.ToEasyUIDatagridJSON(pageObjectVO,MenuUtils.getJSONConfig());
        System.out.println(jsonResult.toString());


        response.getWriter().printf(jsonResult.toString());

    }
}
