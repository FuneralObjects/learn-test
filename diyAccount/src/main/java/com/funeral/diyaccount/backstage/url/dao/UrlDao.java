package com.funeral.diyaccount.backstage.url.dao;

import com.funeral.diyaccount.base.dao.BaseDao;
import com.funeral.diyaccount.base.dao.BaseDaoInfo;
import com.funeral.diyaccount.base.dao.IDaoInfo;
import com.funeral.diyaccount.base.exception.DaoException;
import com.funeral.diyaccount.common.page.pojo.PageObject;
import com.funeral.diyaccount.backstage.url.entity.TDAUrl;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peizangpin on 2017/5/4.
 */
@Repository
public class UrlDao extends BaseDao<TDAUrl,Long> implements IDaoInfo<TDAUrl> {

    @Resource(name = "urlDaoInfo")
    public void intiDaoInfo(BaseDaoInfo<TDAUrl> daoInfo) throws DaoException {
        super.setDaoInfo(daoInfo);
    }

    public PageObject<TDAUrl> queryUrlListByPage(PageObject<TDAUrl> pageObject, JSONObject jsonFilter)throws DaoException {
        if(pageObject == null ){throw new DaoException("Invalid pageObject!");}
        if(jsonFilter == null){throw new DaoException("Invalid jsonFilter");}
        Object urlidObj = jsonFilter.get("urlid");
        Long urlid =  (urlidObj == null || StringUtils.isBlank(String.valueOf(urlidObj)) == true ) ? null : Long.valueOf(String.valueOf(urlidObj));

        List<Object> params = new ArrayList<Object>();
        StringBuffer sql = new StringBuffer("select * from :tableName ");
        sql.replace(sql.indexOf(":tableName"),sql.indexOf(":tableName")+":tableNmae".length(),super.getDaoInfo().getTableName());

        StringBuffer and = new StringBuffer();
        if(urlid != null){
            and.append(" and urlid = ? ");
            params.add(urlid);
        }

        if(StringUtils.isNotBlank(and.toString())){
            sql.append(" where 1 = 1 ");
            sql.append(and);
        }
        pageObject = super.queryListSqlWithPage(sql.toString(), pageObject, params.toArray());

        return pageObject;
    }

    public Long saveUrl(TDAUrl url)throws DaoException{
        Long urlid = super.saveEntity(url);
        return urlid;
    }

    public void delUrl(Long id)throws DaoException{
        super.delEntity(id);
    }

}
