package com.funeral.diyaccount.common.json.date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by peizangpin on 2017/5/23.
 */
public class DateUtils {
    public static JsonConfig getDateJSONConfig(){
        JsonConfig jc = new JsonConfig();
        jc.registerJsonValueProcessor(Date.class, new JsonValueProcessor() {
            public Object processArrayValue(Object o, JsonConfig jsonConfig) {
                return null;
            }
            public Object processObjectValue(String s, Object o, JsonConfig jsonConfig) {

                if(o == null){
                    return "";
                }

                if(o instanceof Date){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    return sdf.format(o);
                }
                return o.toString();
            }

        });

        return jc;
    }
}
