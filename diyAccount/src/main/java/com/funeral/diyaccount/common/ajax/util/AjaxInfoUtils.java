package com.funeral.diyaccount.common.ajax.util;

import com.funeral.diyaccount.common.ajax.pojo.AjaxInfo;
import net.sf.json.JSONObject;

/**
 * Created by peizangpin on 2017/5/15.
 */
public class AjaxInfoUtils {
    public static AjaxInfo initAjaxInfo(AjaxInfo ajaxInfo){
        if(ajaxInfo == null){
            ajaxInfo = new AjaxInfo();
        }
        ajaxInfo.setError(0);
        ajaxInfo.setSuccess("");
        ajaxInfo.setErrorMsg("");
        return ajaxInfo;
    }

    public static String toJSONString(AjaxInfo ajaxInfo){
        if(ajaxInfo == null){
            ajaxInfo = AjaxInfoUtils.initAjaxInfo(ajaxInfo);
        }
        return JSONObject.fromObject(ajaxInfo).toString();
    }

    public static AjaxInfo createAjaxInfo(){
        AjaxInfo ajaxInfo = new AjaxInfo();
        return AjaxInfoUtils.initAjaxInfo(ajaxInfo);
    }
}
