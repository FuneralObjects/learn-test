package com.funeral.diyaccount.common.ajax.pojo;

/**
 * Created by peizangpin on 2017/5/15.
 */
public class AjaxInfo {
    private String success;
    private Integer error;
    private String errorMsg;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
