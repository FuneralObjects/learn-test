package com.funeral.diyaccount.common.param.util;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by peizangpin on 2017/5/22.
 */
public class ParamUtils {

    public static JSONObject getJSONFilter(HttpServletRequest request) {
        String str = request.getParameter("jsonFilter");
        if(StringUtils.isBlank(str)){
            return null;
        }else{
            return JSONObject.fromObject(str);
        }
    }
}
