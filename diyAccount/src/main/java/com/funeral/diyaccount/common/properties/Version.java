package com.funeral.diyaccount.common.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by peizangpin on 2017/5/12.
 */
@Component
public class Version {

    @Value("${version}")
    private String version;

    public String getVersion() {
        return version;
    }

}
