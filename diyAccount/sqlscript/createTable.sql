create sequence seq_da_base_sequence
start with 1000000000000000
increment by 1
maxvalue 9999999999999999;

drop table t_da_object;
create table t_da_object(
	objectid number(16),
	objectcode varchar2(150),
	objectname varchar2(300),
	totalcode varchar2(100),
	parentid number(16),
	objecttype number(3),
	generation number(2),
	hasChildren number(1),
	enable number(1),
	constraint t_da_object_pk primary key(objectid),
	constraint t_da_object_uk unique(objectcode,parentid),
	constraint t_da_object_uk2 unique(totalcode)
);
drop sequence seq_da_object;
create sequence seq_da_object
start with 1000000000000000
increment by 1
maxvalue 9999999999999999;

drop table t_da_menu;
create table t_da_menu(
	menuid number(16),
	menucode varchar2(50),
	menuname varchar2(50),
	menuFolder number(1),
	menuUrl varchar2(1000),
	parentid number(16),
	enable number(1),
	description varchar(1000),
	createTime date not null default sysdate,
	constraint t_da_menu_pk primary key(menuid),
	constraint t_da_menu_uk unique(menucode)
);
